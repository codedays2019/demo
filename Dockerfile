FROM adoptopenjdk/openjdk11

COPY . /usr/src/app

WORKDIR /usr/src/app

RUN ./gradlew clean bootjar --build-cache --no-daemon --info

FROM adoptopenjdk/openjdk11 as builder

COPY --from=0 /usr/src/app/build/libs/*.jar app.jar

CMD java -Djava.security.egd=file:/dev/./urandom -jar app.jar

