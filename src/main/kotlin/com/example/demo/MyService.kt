package com.example.demo

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class MyService {

    @RequestMapping("/hello")
    fun hello() = "Hello World!"
}
