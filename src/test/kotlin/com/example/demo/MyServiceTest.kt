package com.example.demo

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class MyServiceTest {
    val myService = MyService()

    @Test
    fun testHello() {
        val result = myService.hello()
        assertThat(result).isEqualTo("Hello World!")
    }
}
