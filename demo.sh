#!/usr/bin/env bash

########################
# include the magic
########################
. ~/bin/demo-magic.sh


########################
# Configure the options
########################

#
# speed at which to simulate typing. bigger num = faster
#
TYPE_SPEED=30

#
# custom prompt
#
# see http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/bash-prompt-escape-sequences.html for escape sequences
#
DEMO_PROMPT="${GREEN}➜ ${CYAN}\W "
rm -rf  ~/.gradle/caches/build-cache-1

# hide the evidence
clear

pe "open https://build-cache-node-techcamp-demo.cloudapps.iterashift.com/"
pe "clear && cat build.gradle"
pe "clear && cat settings.gradle"
pe "clear && cat Jenkinsfile"
pe "clear"
pe "./gradlew clean check --info"
pe "echo // comment >> src/main/kotlin/com/example/demo/MyService.kt"
pe "git commit -am 'Added a comment' && git push"
curl -X POST http://localhost:8080/job/demo/build --user guido:$(cat jenkins-token.txt)
pe "open http://localhost:8080/job/demo/"
pe "./gradlew clean check --info"
pe "git checkout -b feature/cool"
pe "echo // comment >> src/main/kotlin/com/example/demo/MyService.kt"
pe "git commit -am 'Added a comment' && git push --set-upstream origin feature/cool"
pe "git checkout master && git merge feature/cool && git branch -d feature/cool && git push && git push origin --delete feature/cool"
curl -X POST http://localhost:8080/job/demo/build --user guido:$(cat jenkins-token.txt)
pe "open http://localhost:8080/job/demo/"
p ""

# git rm .gitlab-ci.yml && git commit -m "gitlab ci removed" && git push
